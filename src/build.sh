#!/bin/bash

CODE_PATH="$(dirname "$0")"
CTIME_EXEC="$CODE_PATH/../ctime/ctime"
CTIME_TIMING_FILE="$CODE_PATH/waz.ctm"

$CTIME_EXEC -begin "$CTIME_TIMING_FILE"

mkdir -p ../bin
pushd ../bin

gcc ../src/main.c -std=c99 -o waz -g -w

popd

$CTIME_EXEC -end "$CTIME_TIMING_FILE" $LAST_ERROR
